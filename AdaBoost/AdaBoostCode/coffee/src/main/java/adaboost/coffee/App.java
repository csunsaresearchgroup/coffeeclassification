package adaboost.coffee;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import weka.classifiers.Evaluation;
import weka.classifiers.meta.AdaBoostM1;
import weka.clusterers.SimpleKMeans;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

//exec:java -Dexec.mainClass="adaboost.coffee.App"

public class App
{
	public static void example_adaboost(int i, int j)
	{
		System.out.println("testing: " + i + " x " + j);
		String [] opciones = {"-i","-t","input/train_seg_"+i+"_"+j+".arff",
									"-T","input/test_seg_"+i+"_"+j+".arff","-I","10","-W","weka.classifiers.trees.J48"};
		try
		{
			@SuppressWarnings("resource")
			PrintStream print = new PrintStream(new FileOutputStream("output/test_seg_"+i+"_"+j, true));
			print.println(Evaluation.evaluateModel(new AdaBoostM1(), opciones));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void example_adaboost_g()
	{
		System.out.println("testing: gabor");
		String [] opciones = {"-i","-t","input/train_wks.arff",
									"-T","input/test_wks.arff","-I","10","-W","weka.classifiers.trees.J48"};
		try
		{
			@SuppressWarnings("resource")
			PrintStream print = new PrintStream(new FileOutputStream("output/test_wks", true));
			print.println(Evaluation.evaluateModel(new AdaBoostM1(), opciones));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void example_adaboost()
	{
		int ni = 8;
		int nj = 5;
		for(int i = 1; i <= ni; i++)
			for(int j = 1; j <= nj; j++)
				example_adaboost(i, j);
	}
	public static void example_kmeans()
	{
		 try {
	            // Crear y configurar algoritmo de clustering
	            System.out.println("Crear algoritmo de clustering ...");
	            SimpleKMeans clusterer = new SimpleKMeans();
	            clusterer.setNumClusters(4);
	            clusterer.setMaxIterations(100);

	            // Cargar dataset desde ARFF
	            System.out.println("Cargar dataset ...");            
	            ArffLoader cargadorARRF = new ArffLoader();
	            cargadorARRF.setSource(new FileInputStream("../../weka/iris.2D.arff"));
	            Instances dataset = cargadorARRF.getDataSet();

	            // Entrenar algoritmo de clustering
	            System.out.println("Entrenar algoritmo de clustering ...");
	            clusterer.buildClusterer(dataset);
	            System.out.println();

	            // Identificar el cluster de cada instancia
	            Instance instancia;
	            int cluster;
	            System.out.println("Asignación de instancias a clusters ...");
	            for (int i=0; i < dataset.numInstances(); i++)  {
	                instancia = dataset.instance(i);
	                cluster = clusterer.clusterInstance(instancia);
	                System.out.println("[Cluster "+cluster+"] Instancia: "+instancia.toString());
	            }
	            System.out.println();
	            
	            // Imprimir centroides (sólo con SimpleKMeans [o similares])            
	            int[] tamanoClusters = clusterer.getClusterSizes();
	            Instances centroides = clusterer.getClusterCentroids();
	            Instance centroide;
	            System.out.println("Centroides k-means ...");
	            for(cluster=0; cluster < clusterer.numberOfClusters(); cluster++){
	                centroide = centroides.instance(cluster);
	                System.out.print("Cluster "+cluster+" ("+tamanoClusters[cluster]+" instancias): ");
	                System.out.println("Centroide["+centroide.toString()+"]");                
	            }
	            System.out.println();
	                        
	        } catch (Exception e) {
	            System.out.println("Error en clustering: " + e.getMessage());
	        }
	}
    
	public static void main(String[] args)
    {
		//example_kmeans();
		example_adaboost();
		//example_adaboost_g();
    }
}