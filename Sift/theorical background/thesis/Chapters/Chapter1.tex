\chapter{SIFT}

Scale Invariant Feature Transform (SIFT) es un descriptor de imagen para encontrar correspondencia en imágenes y 
reconocimiento desarrollado por David Lowe (1999, 2004). Este descriptor, así como los descriptores de imágenes 
relacionados se utilizan para una gran cantidad de propósitos en la visión por computadora relacionados con puntos de 
coincidencia entre los diferentes puntos de vista de una escena 3D y reconocimiento de objetos basado en la vista. El 
descriptor SIFT es invariante a traslaciones, rotaciones y transformaciones de escala en el dominio de la imagen y es
robusto para transformaciones moderadas de perspectiva y variaciones de iluminación. Experimentalmente, el descriptor 
SIFT ha demostrado ser muy útil en la práctica para correspondencia de imagenes y reconocimiento de objetos en 
condiciones del mundo real.

El descriptor de SIFT es un método para detectar puntos de interés en una imagen en escala de grises en la cual las 
estadísticas de las direcciones de las gradientes locales de la intensidades de la imagen son acumuladas para dar una 
descripción resumida de las estructuras locales de la imagen en una vecindad local alrededor de cada punto de interés 
con la intención que este descriptor debería ser usado para encontrar los puntos de interés correspondientes entre 
diferentes imágenes \cite{}.

Posteriormente se ha aplicado SIFT a mallas densas (SIFT denso) que ha mostrado un mejor desempeño para tareas como 
categorización de objetos, clasificación de texturas, alineamiento de imágenes, y biometrica. Este descriptor se ha 
extendido de imágenes en escala de grises a imágenes de color y de imágenes de espacio 2D a videos 2+1-D 
espacio-temporales.

\section{Detección de puntos de interés}

Una característica SIFT es una región seleccionada de la imagen (también llamada punto clave) con un descriptor 
asociado. Los puntos clave son extraídos por el detector SIFT y sus descriptores son calculados por el descriptor SIFT.

Un \emph{punto clave SIFT} es una región circular de la imagen con una orientación. Es descrito por un \emph{marco} 
geométrico de cuatro parámetros: las coordenadas $x$, $y$ del centro del punto clave, su escala (el radio de la región) 
y su orientación (un ángulo expresado en radianes)

Los puntos de interés son obtenidos de los extremos de escala-espacio de diferencias de Gausianas en una pirámide de 
diferencias de Gausianas. El concepto de diferencias de Gausianas de paso de bandas de pirámide fue originalmente 
propuesto por Burt y Adelson (1983) y por Crowley y Stern (1984).

Una pirámide Gausiana es construida de la imagen de entrada mediante suavizado y submuestreo repetido y una pirámide de 
de diferencias de Gausianas se calcula de las diferencias entre los niveles adyacentes en la pirámide Gausiana. 
Entonces, los puntos de interés son obtenidos de los puntos en los que los valores de la diferencia de Gausianas asumen 
extremos con respecto tanto a las coordenadas espaciales en el dominio de la imagen como el nivel de escala en la 
pirámide.

\subsection{Scale-space Extrema Detection}

Se calcula el Laplaciano de la Gaussiana (LoG) de la imagen con diferentes valores de $\sigma$. LoG actúa como un 
detector de burbujas, que detecta burbujas de diferentes tamaños debido el cambio en $\sigma$. En resumen $\sigma$ 
actúa como un parámetro de escalamiento. Por ejemplo, el kernel Gaussiano  con un valor pequeño de $\sigma$ da valores 
altos para detalles pequeños mientras que el kernel Gaussiano con un valor grande de $\sigma$ trabaja bien para 
detalles mas grandes. De manera que se puede encontrar el máximo local en la escala y en el espacio lo que nos da una 
lista de valores $(x,y,\sigma)$ que significa que existe un potencial punto de interés en $(x,y)$ en la escala $\sigma$.

LoG es algo costoso, por lo que SIFT usa diferencias de Gaussianas (DoG) que es una aproximación de LoG. DoG se obtiene 
se obtiene como la diferencia de blur Gaussiano de una imagen con dos $\sigma$ diferentes, sean $\sigma$ y $k_\sigma$. 
Este proceso se hace para diferentes octavas de la imagen en pirámide Gausiana, este proceso se aprecia en la 
Figura~\ref{fig:siftdog}.
$$\mathrm{DoG}_{\sigma(o,s)} = I_{\sigma(o,s+1)} - I_{\sigma(o,s)}$$
\begin{figure}[bth]
\centering
\includegraphics[width=.5\linewidth]{sift_dog}
\caption[DoG]{Diferencia de Gausianas}\label{fig:siftdog}
%\tiny \textbf{Fuente:} OpenCV
\end{figure}

Luego que DoG ha sido calculado, se busca en las imágenes extremos locales sobre la escala y el espacio. Por ejemplo, 
un pixel en una imagen es comparado con sus 8 vecinos así como con los 9 pixeles en la escala siguiente y con los 9 de 
la escala previa como se muestra en la Figura~\ref{fig:sift_local_extrema}. Si el pixel evaluado es extremo local, es 
un potencia punto de interés. 

En cuanto a los diferentes parámetros, el autor muestra algunos datos empíricos que son: numero de octavas = 4, numero 
de niveles de escala = 5, $\sigma$ = $1.6$, $k$ = $\sqrt{2}$.

\begin{figure}[bth]
\centering
\includegraphics[width=.5\linewidth]{sift_local_extrema}
\caption[Extremos Locales]{Vecindad considerada para el cálculo de extremos locales}\label{fig:sift_local_extrema}
%\tiny \textbf{Fuente:} OpenCV
\end{figure}

\subsection{Localización de los puntos de interés}

Luego de que los potenciales puntos de interés fueron localizados, tienen que ser refinados para obtener resultados mas 
precisos. Se utiliza la expansión de la serie de Taylor en escala y espacio para obtener una mas precisa localización 
de extremos, y si la intensidad en este extremo es menor que un umbral (0.03 según el autor), es rechazado.

DoG tiene mayor respuesta en los bordes, de manera que los bordes también tienen que ser removidos. Para esto se 
utiliza un concepto similar al del detector de bordes de Harris. Se calcula la curvatura principal usando una matriz 
Hessiana ($H$) de $2\times 2$. Se sabe del detector de esquinas de Harris que para bordes, un eigen valor en mayor que 
el otro. Si esta proporción es mayor que un umbral, este punto de interés es descartado. El autor propone umbral = 10.

\subsection{Orientación}

Se asigna una orientación a cada punto de interés para lograr invariancia a la rotación. Se toma la vecindad alrededor 
de la localización del punto de interés dependiendo de la escala, sea $L$ la imagen suavizada por la Gausiana con la
escala del punto de interés, y calcula la magnitud de la gradiente y su dirección en esa región como se muestra en las 
ecuaciones a continuación. 

$$m(x,y) = \sqrt{(L(x+1,y)-L(x-1,y))^2 + (L(x,y+1)-L(x,y-1))^2}$$
$$\theta (x,y) = \arctan^{-1}((L(x,y+1)-L(x,y-1))/(L(x+1,y)-L(x-1,y)))$$

Se forma un histograma de las gradientes de la orientaciones de los puntos de muestra en una región alrededor del punto 
de interés. El histograma de orientación es de tamaño 36 cubriendo el rango de orientaciones de 360. Cada muestra 
agregada al histograma es ponderada por la magnitud de su gradiente y por una ventana circular ponderada por la 
Gausiana con un $\sigma$ que es 1.5 veces la escala del punto de interés.

Los picos en el histograma de orientación corresponden a las direcciones dominantes de las gradientes locales. Se 
detecta el pico mas alto en el histograma, y luego cualquier otro pico que esta en el 80\% del pico mas alto es usado 
también para crear un punto de interés con esa orientación. Por lo tanto para ubicaciones con múltiples picos de 
similar magnitud, habrán múltiples puntos de interés creados en la misma posición y escala pero con diferentes 
orientaciones.
%Sólo alrededor del 15\% de puntos se les asigna múltiples orientaciones, pero ellas contribuyen
Finalmente se ajusta una parábola a los 3 valores del histograma mas cercanos a cada pico para interpolar la posición 
más precisa del pico.

\begin{figure}[bth]
\centering
\includegraphics[width=.8\linewidth]{sift-orient}
\caption[Histograma de orientaciones]{Selección de la dirección en el histograma de 
orientaciones}\label{fig:sift_orient}
%\tiny \textbf{Fuente:} OpenCV
\end{figure}

\section{Descriptor de Sift}

Un descripto de Sift es un histograma 3-D de las gradientes de la imagen que caracterizan el punto de interés. La 
gradiente en cada pixel es tomada como una muestra de la vector de 3 dimensiones formado por la localización del pixel 
y la orientación del gradiente. Las muestras son ponderadas por una Gausiana con varianza de 1.5 veces la escala del 
punto de interés y acumuladas en un histograma 3D $h$, las gradientes son rotadas de acuerdo a la orientación del punto 
de interés.

Se utilizan normalmente un conjunto de 16 histogramas, alineados en una grilla de 4x4, cada uno con 8 orientaciones, 
una por cada dirección principal del compás y una por cada punto medio entre esas direcciones, resultando un vector de 
128 elementos.	

\begin{figure}[bth]
\centering
\includegraphics[width=.95\linewidth]{sift-descr-easy.png}
\caption[Descriptor de SIFT]{Histograma de gradientes}\label{fig:sift_orient}
%\tiny \textbf{Fuente:} OpenCV
\end{figure}
