TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11

SOURCES += main.cpp


LIBS += -lopencv_nonfree -lopencv_features2d -lopencv_imgproc -lopencv_highgui -lopencv_core -lopencv_flann
