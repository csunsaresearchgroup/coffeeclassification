SifBoW usage:

```
#!bash
$ ./SiftBow <images_dir> <num_features>
```

example
```
#!bash
$ ./SiftBow ../cafeFondoBlancoClass 16
```

To generate different vector sizes use 
```
#!bash
$ ./computeFeatures.sh
```