#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <dirent.h>
#include <fstream>

using namespace std;
typedef vector< pair<string,vector<string> > > data_t;

void getFilesInDir(string & dir_path,vector<string> & files)
{
    DIR * subdir;
    struct dirent *ent;
    subdir = opendir (dir_path.c_str());
    dir_path += '/';
    while ((ent = readdir (subdir)) != NULL)
    {
        if( ent->d_type==DT_REG )
        {
            files.push_back(dir_path+ent->d_name);
        }
    }
    closedir (subdir);
}


size_t getFileNames(string dir_base, vector< pair<string,vector<string> > > & types)
{
    if(dir_base[dir_base.length()-1]!='/')
    {
        dir_base+="/";
    }
    DIR *dir;
    struct dirent *ent;
    string full_path;
    size_t c = 0,count = 0;
    if ((dir = opendir (dir_base.c_str())) != NULL) {
      while ((ent = readdir (dir)) != NULL) {
          if( ent->d_type==DT_DIR && c>1 )
          {
              full_path = dir_base+ent->d_name;
              vector<string> files;
              getFilesInDir(full_path,files);
              types.push_back(make_pair(ent->d_name,files));
              count+=files.size();
          }
          c++;
      }
      closedir (dir);
    } else {
      /* could not open directory */
      cerr<<"Could not open directory :"<<dir_base<<endl;
      return -1;
    }
    return count;
}

void saveToArffFile(string filename, vector<pair<string, cv::Mat> > descs,vector<string> type_names,size_t num_features);

int main(int argc, const char* argv[])
{

    if(argc<2)
    {
        clog<<"Usage : SiftBoW <images database path> <number of features>";
        return 0;
    }

    //vector<string> image_paths;
    vector< pair<string,vector<string> > > image_types;

    cout<<"Total Images : "<<getFileNames(argv[1],image_types)<<endl;
    cout<<"Total Types : "<<image_types.size()<<endl;
    cout<<"Computing dictionary ...";
    cv::Mat input;
    //To store the keypoints that will be extracted by SIFT
    vector<cv::KeyPoint> keypoints;
    //To store the SIFT descriptor of current image
    cv::Mat descriptor;
    //To store all the descriptors that are extracted from all the images.
    cv::Mat featuresUnclustered;
    //The SIFT feature extractor and descriptor
    cv::SiftDescriptorExtractor detector;

    for(pair<string,vector<string> > & type: image_types)
    {
        for(string & filename: type.second)
        {
            //cout<<filename<<endl;
            input = cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale
            //detect feature points
            detector.detect(input, keypoints);
            //compute the descriptors for each keypoint
            detector.compute(input, keypoints,descriptor);
            //put the all feature descriptors in a single Mat object
            featuresUnclustered.push_back(descriptor);
        }
    }

    //the number of bags
    //int dictionarySize=16;
    string dictionarySizeStr(argv[2]);
    size_t dictionarySize = stoi(dictionarySizeStr);
    //define Term Criteria
    cv::TermCriteria tc(CV_TERMCRIT_EPS,1000,0.001);
    //retries number
    int retries=1;
    //necessary flags
    int flags=cv::KMEANS_PP_CENTERS;
    //Create the BoW (or BoF) trainer
    cv::BOWKMeansTrainer bowTrainer(dictionarySize,tc,retries,flags);
    //cluster the feature vectors
    cv::Mat dictionary=bowTrainer.cluster(featuresUnclustered);
    //store the vocabulary
    cv::FileStorage fs("dictionaries/dictionary_"+dictionarySizeStr+".yml", cv::FileStorage::WRITE);
    fs << "vocabulary" << dictionary;
    fs.release();
    cout<<"Ok"<<endl;

    cout<<"Computing feature vectors ...";

    /*cv::Mat dictionary;
    cv::FileStorage fs("dictionaries/dictionary_"+dictionarySizeStr+".yml", cv::FileStorage::READ);
    fs["vocabulary"] >> dictionary;
    fs.release();*/

    //create a nearest neighbor matcher
    cv::Ptr<cv::DescriptorMatcher> matcher(new cv::FlannBasedMatcher);
    //create Sift feature point extracter
    cv::Ptr<cv::FeatureDetector> detector_ptr(new cv::SiftFeatureDetector());
    //create Sift descriptor extractor
    cv::Ptr<cv::DescriptorExtractor> extractor(new cv::SiftDescriptorExtractor);
    //create BoF (or BoW) descriptor extractor
    cv::BOWImgDescriptorExtractor bowDE(extractor,matcher);
    //Set the dictionary with the vocabulary we created in the first step
    bowDE.setVocabulary(dictionary);


    //To store the BoW (or BoF) representation of the image
    cv::Mat bowDescriptor;
    vector<pair<string, cv::Mat> > descriptors;
    vector<string> type_names;
    for(pair<string,vector<string> > & type: image_types)
    {
        type_names.push_back(type.first);
        for(string & filename: type.second)
        {
            cv::Mat img=cv::imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
            detector_ptr->detect(img,keypoints);
            //extract BoW (or BoF) descriptor from given image
            bowDE.compute(img,keypoints,bowDescriptor);
            descriptors.push_back(make_pair(type.first,bowDescriptor));
        }
    }

    saveToArffFile("arff/coffeeSIFT_BoW_"+dictionarySizeStr+".arff",descriptors,type_names,dictionarySize);
    cout<<"Ok"<<endl;

    cout<<"Done"<<endl;
    return 0;
}

void saveToArffFile(string filename, vector<pair<string, cv::Mat> > descs,vector<string> type_names,size_t num_features)
{
    ofstream arffFile(filename);
    if(!arffFile.is_open()) return;

    arffFile<<"%% 1. Title: Coffee Sift BoW Features Database"<<endl
            <<"%% 2. Sources:"<<endl
            <<"%%          (a)"<<endl
            <<"%%          (b)Generated by: José L. Valencia G."<<endl<<endl

           <<"@RELATION coffeeSiftBoW"<<endl<<endl;

    for(size_t i = 0; i<num_features; i++)
    {
        arffFile<<"@ATTRIBUTE SIFT_BoW_F_"<<i<<" NUMERIC"<<endl;
    }
    arffFile<<endl
        <<"@ATTRIBUTE class {";
    bool first = true;
    for(string & c : type_names)
    {
        if(first)
        {
            arffFile<<c; //<<endl;
            first = false;
        }
        else
        {
            arffFile<<", "<<c;
        }
    }
    arffFile<<"}"<<endl<<endl
            <<"@DATA"<<endl;
    for(pair<string, cv::Mat> & d : descs)
    {
        arffFile<<format(d.second,"csv")<<","<<d.first<<endl;
    }
    arffFile.close();
}

// Add results to image and save.
/*cv::Mat output;
cv::drawKeypoints(input, keypoints0, output);
cv::imwrite("sift_result.jpg", output);*/

