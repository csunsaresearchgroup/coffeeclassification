//Source: http://nion.modprobe.de/tmp/mkdir.c
//Create a directory 'recursively'
//ToDO: add error messages
#include <sys/stat.h>

static void _mkdir(const char* path)
{
	char opath[256];
	char* p;
	size_t len;

	strncpy(opath,path,sizeof(opath));
	len=strlen(opath);
	if(opath[len-1]=='/')
		opath[len-1]='\0';

	for(p=opath;*p;p++)
		if(*p=='/')
		{
			*p='\0';
			if(access(opath,F_OK))
				mkdir(opath,S_IRWXU);
			*p ='/';
		}

	if(access(opath,F_OK)) /* if path is not terminated with / */
		mkdir(opath,S_IRWXU);
}
