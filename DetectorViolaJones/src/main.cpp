//#include <log4cplus/loglevel.h>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include "include/Common.h"

namespace opts=boost::program_options;

int main(int argc,char** argv)
{
	/*log4cplus::initialize();
	log4cplus::BasicConfigurator::doConfigure();
	log4cplus::Logger logger=log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("main"));*/

	opts::options_description desc("Usage: ./detector [options]\n\nAllowed options");
	desc.add_options()
		//("help,h","Produce this help message")
		//("verbose,v",opts::value<int>()->default_value(0),"Set verbosity level: 0-5 (default 0)")
		("input_image,i",opts::value<std::string>(),"Set input image (mandatory)")
		("cascade,c",opts::value<std::string>()->default_value("../haarcascades/haarcascade_coffee.xml"),"Set cascade file")
		("outputdir,o",opts::value<std::string>()->default_value("../output"),"Set output directory")
		;
	
	opts::variables_map vm;

	try
	{
		opts::store(opts::parse_command_line(argc,argv,desc),vm);

		if(vm.count("input_image"))
		{
			std::string input_image=vm["input_image"].as<std::string>();
			//LOG std::cout<<input_image<<std::endl;
			
			cv::CascadeClassifier cascade;

			if(!cascade.load(vm["cascade"].as<std::string>()))
			{
				std::cout<<"ERROR: Could'nt load classifier cascade."<<std::endl;
				std::cout<<desc<<std::endl;
				return -1;
			}
			
			int i=0;
			cv::Mat image;
			
			image=cv::imread(input_image);
			if(image.empty())
			{
				std::cout<<"ERROR: Could'nt read image."<<std::endl;
				return -1;
			}

			cv::Mat gray;
			cv::cvtColor(image,gray,CV_BGR2GRAY);
			//cv::equalizeHist(gray,gray);
			
			std::vector<cv::Rect> objects;
			cascade.detectMultiScale(gray,objects,1.3,8);

			double avgw=0,avgh=0;
			for(std::vector<cv::Rect>::const_iterator r=objects.begin();r!=objects.end();r++)
			{
				avgw+=r->width;
				avgh+=r->height;
			}

			avgw/=objects.size();
			avgh/=objects.size();

			unsigned found=input_image.find_last_of("/");
			unsigned foundDot=input_image.find_last_of(".");
			
			//std::string output_dir=vm["outputdir"].as<std::string>()+"/"+input_image.substr(found+1,foundDot-found-1);
			std::string output_dir=vm["outputdir"].as<std::string>();
			std::string objoutput_dir=output_dir+"/detectedObjects";

			std::cout<<"Creando: "<<output_dir<<std::endl;
			_mkdir(output_dir.c_str());
			_mkdir(objoutput_dir.c_str());
			
			cv::Mat copyImg=image.clone();
			for(std::vector<cv::Rect>::const_iterator r=objects.begin();r!=objects.end();r++,i++)
			{
				cv::Mat smallImgROI;
				cv::Mat smallImgROIRGB;
				cv::Point center;

				//if(r->width>=ww*0.8 && r->height>=hh*0.8 && r->width<=ww*1.3 && r->height<=hh*1.3)
				if(r->width>=avgw*0.8 && r->height>=avgh*0.8)
				{
					cv::rectangle(copyImg,cv::Point(r->x,r->y),cv::Point((r->x+r->width-1),(r->y+r->height-1)),CV_RGB(0,0,255),2);

					int len=50;
					cv::Size s=image.size();
					int x1=r->x-2*len;
					int y1=r->y-2*len;
					int x2=r->width+4*len;
					int y2=r->height+4*len;
					if(x1<0) x1=0;
					if(y1<0) y1=0;
					if(x2>=(s.width-x1))
					{
						x1=r->x;
						x2=r->width;
					}
					if(y2>=(s.height-y1))
					{
						y1=r->y;
						y2=r->height;
					}

					smallImgROI=gray(cv::Rect(x1,y1,x2,y2));
					smallImgROIRGB=image(cv::Rect(x1,y1,x2,y2));

					std::vector<std::vector<cv::Point>> myContours;
					cv::Mat contourOutput=smallImgROI.clone();

					cv::threshold(contourOutput,contourOutput,0,255,cv::THRESH_BINARY+cv::THRESH_OTSU);
					//Okcv::threshold(contourOutput,contourOutput,150,255,cv::THRESH_BINARY);
					//OkOkcv::adaptiveThreshold(contourOutput,contourOutput,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY,11,2);
					cv::bitwise_not(contourOutput,contourOutput);
					
					cv::findContours(contourOutput,myContours,cv::RETR_LIST,cv::CHAIN_APPROX_NONE);
					
					int iLargestContour=0;
					int largest_area=0;
					cv::Rect bounding_rect;
					for(size_t I=0;I<myContours.size();++I)
					{
						double a=contourArea(myContours[I],false);
						if(a>largest_area)
						{
							largest_area=a;
							iLargestContour=I;
							bounding_rect=boundingRect(myContours[I]);
						}
					}
					
					cv::Mat mask=cv::Mat::zeros(smallImgROI.rows,smallImgROI.cols,CV_8UC1);
					cv::RotatedRect rotatedRect=cv::fitEllipse(cv::Mat(myContours[iLargestContour]));
					cv::ellipse(mask,rotatedRect,cv::Scalar(255),-1);
					
					cv::Mat crop(smallImgROI.rows,smallImgROI.cols,CV_8UC3);
					crop.setTo(cv::Scalar(255,255,255));
					smallImgROIRGB.copyTo(crop,mask);

					cv::imwrite(output_dir+"/"+input_image.substr(found+1,foundDot-found-1)+std::to_string(i)+".jpg",crop);
				}
				else
					cv::rectangle(copyImg,cv::Point(r->x,r->y),cv::Point(r->x+r->width-1,r->y+r->height-1),CV_RGB(0,255,0),2);
			}
			cv::imwrite(objoutput_dir+"/"+input_image.substr(found+1),copyImg);
		}
		else
		{
			std::cout<<desc<<std::endl;
			return true;
		}

		opts::notify(vm);
	}
	catch(std::exception &e)
	{
		//LOG4CPLUS_ERROR(logger,LOG4CPLUS_TEXT("Exception: ")<<e.what());
		std::cout<<e.what()<<std::endl;
		std::cout<<"Use \"./detector -h\" for help\n\n";
		return -1;
	}
	
/*	if(vm.count("verbose"))
	{
		switch(vm["verbose"].as<int>())
		{
			case 0:
				logger.setLogLevel(log4cplus::DEBUG_LOG_LEVEL);
				break;
			case 1: 
				logger.setLogLevel(log4cplus::INFO_LOG_LEVEL);
				break;
			case 2: 
				logger.setLogLevel(log4cplus::WARN_LOG_LEVEL);
				break;
			case 3:
				logger.setLogLevel(log4cplus::ERROR_LOG_LEVEL);
				break;
			case 4:
				logger.setLogLevel(log4cplus::FATAL_LOG_LEVEL);
				break;
			case 5:
				logger.setLogLevel(log4cplus::OFF_LOG_LEVEL);
				break;
			default:
				logger.setLogLevel(log4cplus::DEBUG_LOG_LEVEL);
		}
	}

	cof::PreProcessingOptions opts(vm.count("invert"),vm.count("close"),vm.count("threshold"),vm.count("normalize"));

	if(vm.count("extractor")&&vm.count("dataset")&&vm.count("arff"))
	{
		cof::FeatureExtractor<double>* fe=nullptr;

		if(vm["extractor"].as<std::string>().compare("cnejd")==0)
			fe=new cof::ComplexNetworkExtractorJointDegree<double>();
		else if(vm["extractor"].as<std::string>().compare("cned")==0)
			fe=new cof::ComplexNetworkExtractorDegree<double>();
		else if(vm["extractor"].as<std::string>().compare("sc")==0)
			fe=new cof::ShapeCircularity<double>();

		cof::Dataset dat(vm["dataset"].as<std::string>());

		if(fe==nullptr)
		{
			LOG4CPLUS_FATAL(logger,LOG4CPLUS_TEXT("Extractor is not valid: ")<<vm["extractor"].as<std::string>()<<"\n");
			return -1;
		}

		std::string strOpts;
		if(vm.count("invert"))
			strOpts+="_inv";
		if(vm.count("close"))
			strOpts+="_cl";
		if(vm.count("threshold"))
			strOpts+="_ts";
		if(vm.count("normalize"))
			strOpts+="_nor";

		cof::ArffFormatter<double> formatter(dat,*fe,dat.getDirName()+"-"+vm["extractor"].as<std::string>()+strOpts+".arff");
		formatter.process(opts);
	}
	else if(vm.count("dataset")&&vm.count("contours"))
	{
		cof::Dataset dataset(vm["dataset"].as<std::string>());
		auto dat=dataset.getDataset();

		for(auto group:dat)
		{
			for(auto imageName:group.second)
			{
				cof::Image im(dataset.getDirName()+"/"+group.first+"/"+imageName,opts);
				im.processContour();

				cv::Mat contourImage(im.getSize(),CV_8UC3,cof::BLACK);
				for(size_t I=0;I<im.getContours().size();++I)
					cv::drawContours(contourImage,im.getContours(),I,cof::COLORS[I%3]);

				cv::imshow("Input Image",im.getImage());
				cv::imshow("Contours",contourImage);
				cv::waitKey(0);
			}
		}
		
	}
	else if(vm.count("image")&&vm.count("contours"))
	{
		cof::Image imagenTest(vm["image"].as<std::string>(),opts);
		imagenTest.processContour();
		cv::Mat contourImage(imagenTest.getSize(),CV_8UC3,cof::BLACK);
		cv::drawContours(contourImage,imagenTest.getContours(),imagenTest.getLargestContour(),cof::GREEN);
		cv::imshow("Input Image",imagenTest.getImage());
		cv::imshow("Contours",contourImage);
		cv::waitKey(0);
	}
	else
	{
		LOG4CPLUS_ERROR(logger,LOG4CPLUS_TEXT("Some argument is required but missing....\n"));
		std::cout<<desc<<std::endl;
	}
*/
	return 0;
}
