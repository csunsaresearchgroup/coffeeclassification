function generate_gabor_weka_files(m, p)
L = getLabels('datacafe');
[mtrain,ltrain,mtest,ltest] = train_test_divide(m,L,p);
toWeka('train_wks',mtrain,ltrain);
toWeka('test_wks',mtest,ltest);