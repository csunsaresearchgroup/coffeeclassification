function generate_weka_files(data,p)
    L = getLabels('datacafe_fondoblanco');
    for i=1:length(data)
        for j=1:length(data{i})
            [mtrain,ltrain,mtest,ltest] = train_test_divide(data{i}(j).mat,L,p);
            strain = sprintf('train2_%d_%d',i,j);
            stest = sprintf('test2_%d_%d',i,j);
            toWeka(strain,mtrain,ltrain);
            toWeka(stest,mtest,ltest);
            data{i}(j)
        end
    end
