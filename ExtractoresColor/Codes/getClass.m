function n = getClass(s)

p = find(s == '/');
l = length(p);

if l > 0
    s = s(1,p(l)+1:length(s));
end

n = '';
for i=1:length(s)
    if s(i) >= '0' & s(i) <= '9'
        break;
    end
    n = [n s(i)];
end