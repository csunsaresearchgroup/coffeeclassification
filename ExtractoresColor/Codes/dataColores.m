
% Extrae el vector de caracteristicas de cada imagen
% D: parametro de division de H  0 360.
% I: Parámetro de division de S y V de 0 a 1

% D=10 I=0.25
function [M time]=dataColores(D, I) 
	tic;
    images=fopen('datacafe_fondoblanco','r');

	
	tam=fscanf(images,'%d');
	for i=1:tam
        %tic
		image=fgetl(images);
        image=imread(image);
        Mask=mascaraCafe(image,230);
		M(i,:)=probabilidades(colorImage2(image,Mask,D,I));
        %toc
	end

	time=toc;