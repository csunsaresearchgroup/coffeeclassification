function Vec=colorImage2(I,M,d,i)

	I=rgb2hsv(I);

	H=0+d:2*d:360-d;
	H=H/360;
	
	k=1;
	V=0:i:1;
	S=0:i:1;
	V(1)=V(1)-0.0001;
	S(1)=S(1)-0.0001;

	for v=1:length(V)-1
		for s=1:length(S)-1
			F{k}=find(M~=0 & (I(:,:,1)<=H(1) | I(:,:,1)>H(length(H))) & I(:,:,3)>V(v) & I(:,:,3)<=V(v+1) & I(:,:,2)>S(s) & I(:,:,2)<=S(s+1));
			k=k+1;
		end
	end
	
	for h=1:length(H)-1
		for v=1:length(V)-1
			for s=1:length(S)-1
				F{k}=find(M~=0 & I(:,:,1)>H(h) & I(:,:,1)<=H(h+1) & I(:,:,3)>V(v) & I(:,:,3)<=V(v+1) & I(:,:,2)>S(s) & I(:,:,2)<=S(s+1));
				k=k+1;
			end
		end
	end
	
	for i=1:length(F)
		Vec(i)=length(F{i});
	end