function M=mascaraCafe(I,D)
    M=I(:,:,1);
    M(find(I(:,:,1)>D & I(:,:,2)>D & I(:,:,3)>D))=0;
    M(find(M>0))=255;
