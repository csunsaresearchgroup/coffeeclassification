function [data time]=dataGabor()
	
	images=fopen('datacafe','r');
	tic
	tam=fscanf(images,'%d');
	for i=1:tam
		image=fgetl(images);
		image=rgb2gray(imread(image));
		image = resizem(image,[200 200]);
		[A B C D]=gaborfilter2(image,2,2,180,270);
		%size(image)
		data(i,:)=log(fft(reshape(C,1,prod(size(C)))));
	end
	
	data=data/max(max(data));
	time=toc;

