function [ D ] = divisores( n )
%Retorna todos los divisores de n

j=1;
for i=1:n
    if(mod(n,i)==0)
        D(j)=i;
        j=j+1;
    end
end
