function l = getLabels(file)
images=fopen(file,'r');
tam=fscanf(images,'%d');

for i = 1:tam
    s = fgetl(images);
    l{i} = getClass(s);
end