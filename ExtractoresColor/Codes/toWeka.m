function toWeka(file, M, L)

path = '../../AdaBoost/AdaBoostCode/coffee/input/';
[path file '.arff']
f = fopen([path file '.arff'],'w');

fprintf(f, '@relation %s\n\n', file);

l = unique(L,'stable');

s = size(M);

for i = 1:s(2)
    fprintf(f, '@attribute a%d numeric\n', i);
end

str = ''; 
for i = 1:length(l) - 1
    str = [str l{i} ', '];
end

str = [str l{length(l)}];

fprintf(f, '@attribute class {%s}\n\n', str);


fprintf(f, '@data\n\n');

for i = 1:s(1)
    str = '';
    for j = 1:s(2)
        str = [str sprintf('%.8f,', M(i,j))];
    end
    fprintf(f, '%s%s\n', str, L{i});
end



