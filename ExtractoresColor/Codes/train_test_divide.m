function [mtrain,ltrain,mtest,ltest] = train_test_divide(M,L,p)
	[C,IA,IC] = unique(L,'stable');
	IR = [IA(2:length(IA));length(L)+1];
	CC = IR - IA;
	ctrain = round(CC*p);
	
	r = 1; %train
	s = 1; %test
	k = 1;
	
	for i = 1:length(ctrain)
		index = randperm(CC(i), ctrain(i));
		N = zeros(1,CC(i));
		N(index) = 1;
		
		for j = 1:CC(i)
			if N(j) == 1
				mtrain(r,:) = M(k,:);
				ltrain{r} = L{k};
				r = r + 1;
			else
				mtest(s,:) = M(k,:);
				ltest{s} = L{k};
				s = s + 1;
			end
			k = k + 1;
		end
    end
end
