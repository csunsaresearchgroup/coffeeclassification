function data = pruebas_color()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% Hallar divisores de un numero y para cada uno hacer pruebas

aH = [10 15 20 30 45 60 90 180]
aSV = [0.05 0.1 0.2 0.25 0.5]




if matlabpool('size') == 0 % checking to see if my pool is already open
    matlabpool open 8
end

matlabpool('size')

data = cell(length(aH),1);
fprintf('%10s %10s %10s %10s\n', 'dH', 'dSV', 'time', 'dim');
parfor i=1:length(aH)
    for j=1:length(aSV)
        data{i}(j).dH=aH(i);
        data{i}(j).dSV=aSV(j);
        
        fprintf('%10d %10.2f', data{i}(j).dH, data{i}(j).dSV)
        
        [data{i}(j).mat data{i}(j).time] = dataColores(aH(i),aSV(j));
        
        fprintf('%10.2fs %10d\n', data{i}(j).time, length(data{i}(j).mat));
        
		%file = sprintf('data/data-%d-%.2f.mat', data(d).dH, data(d).dSV);
		%save(file, 'data');
    end
end


%fprintf('TIME TOTAL: %4.4fs\n', time);
save('data/data_fondoblanco.mat', 'data');
%save('time', 'time');

%La generación de los vectores deberia tomar 13h:43m:49.8s
end

